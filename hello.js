
// print "Hello, $name!"
function hello(name) {
	// Use string substitution	
	console.log("Hello, %s!", name);
}

// export hello as a module
module.exports = hello;
